var path = require('path');
var redis = require("redis"),
    rClient = redis.createClient();

const io = require('socket.io')({
  path: '/pain',
  serveClient: false,
});

const server = require('http').createServer();

io.attach(server, {
  pingInterval: 10000,
  pingTimeout: 5000,
  cookie: false
});

server.listen(3000, function () {
  console.log('Panik Counting enabled!');
});

  let countCache = 0;

  function updateCache(key, count) {
    countCache = count;
  }

  function getCounts() {
    rClient.get("panik.total-count", function(err, count) {
      updateCache("panik.total-count", count);
    });
  }

  // Keep updating counting
  var countUpdateInterval = setInterval(function () {
      getCounts();      
  }, 2000);

// On CONNECT
io.on('connection', function (socket) {

  socket.emit('updateCounts', countCache);

  // Push updated count
  var countBroadcast = setInterval(function () {
    socket.emit('updateCounts', countCache);
    // Update count of connected clients for shitty 'users online'
    socket.emit('updateConnectedCount', io.engine.clientsCount);
  }, 2000);

  socket.on('setNewCount', function () {
      rClient.incr("panik.total-count", function(err, count) {
          // socket.emit('getUpdatedCount', count);
      });
  });

  // On DISCONNECT
  socket.on('disconnect', function () {
    clearInterval(countBroadcast);
  });

});
